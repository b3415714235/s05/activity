from abc import ABC, abstractclassmethod

class Person(ABC):

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department



    #getters:
    def get_firstName(self):
    	print(f"firstName: {self._firstName}")

    def get_lastName(self):
    	print(f"lastName: {self._lastName}")

    def get_email(self):
    	print(f"Email: {self._email}")

    def get_department(self):
    	print(f"department: {self._department}")

    #setters:
    def set_firstName(self,firstName):
    	self._firstName = firstName
    def set_lastName(self,lastName):
    	self._lastName = lastName
    def set_email(self,email):
    	self._email = email
    def set_department(self,department):
    	self._department = department

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def login(self):
    	return f"{self._email} has logged in"
    def logout(self):
    	return f"{self._email} has logged out"

    def addRequest(self):
    	return(f"Request has been added")
    def checkRequest(self):
    	pass
    def addUser(self):
    	pass
    

class TeamLead(Person):
	def __init__(self, firstName,lastName,email,department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._members = []

	#getters:
	def get_firstName(self):
		print(f"firstName: {self._firstName}")

	def get_lastName(self):
		print(f"lastName: {self._lastName}")

	def get_email(self):
		print(f"Email: {self._email}")

	def get_department(self):
		print(f"department: {self._department}")
	def get_members(self):
		return self._members



	#setters:
	def set_firstName(self,firstName):
		self._firstName = firstName
	def set_lastName(self,lastName):
		self._lastName = lastName
	def set_email(self,email):
		self._email = email
	def set_department(self,firstName):
		self._department = department

	def set_department(self,firstName):
		self._members = members


	def getFullName(self):
	    return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		pass
	def checkRequest(self):
		print(f"Checking request")
	def addUser(self):
		pass

	def login(self):
	       return f"{self._email} has logged in."

	def logout(self):
	       return f"{self._email} has logged out."

	def addMember(self, employee):
    		self._members.append(employee)
    		return(f"{employee.getFullName()} added to the team")



class Admin(Person):
    def __init__(self, firstName,lastName,email,department):
    	super().__init__()
    	self._firstName = firstName
    	self._lastName = lastName
    	self._email = email
    	self._department = department
    	

    def get_firstName(self):
    	print(f"firstName: {self._firstName}")

    def get_lastName(self):
    	print(f"lastName: {self._lastName}")

    def get_email(self):
    	return(f"Email: {self._email}")

    def get_department(self):
    	return(f"department: {self._department}")

    #setters:
    def set_firstName(self,firstName):
    	self._firstName = firstName
    def set_lastName(self,lastName):
    	self._lastName = lastName
    def set_email(self,email):
    	self._email = email
    def set_department(self,department):
    	self._department = department

    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")

    def addRequest(self):
    	pass
    def checkRequest(self):
    	pass
    def addUser(self):
    	return(f"User has been added")

    def login(self):
    	return"{self._email} has logged in."
    def logout(self):
    	return"{self._email} has logged out."



class Request():
	def __init__(self,name,requester,dateRequested):
		self._name=name
		self._requester=requester
		self._dateRequested=dateRequested
		self._status=""
	def get_name(self):
		print(f"name: {self._name}")

	def get_requester(self):
		print(f"Email: {self._email}")

	def get_dateRequested(self):
		print(f"Date Requested: {self._dateRequested}")

	def get_status(self):
		return(f"status: {self._status}")

	#setters:
	def set_name(self,firstName):
		self._name=name
	def set_requester(self,lastName):
		self._requester=requester
	def set_dateRequested(self,email):
		self._email = email
	def set_status(self,status):
		self._status=status
		return(f"{status}")



	def updateRequest(self):
		return(f"Request {self._name} has been requested.")
	def closeRequest(self):
		return(f"Request {self._name} has been closed.")
	def cancelRequest(self):
		return(f"Request{self._name} has been cancelled.")




employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())
assert admin1.addUser() == "User has been added"
req2.set_status("closed")
print(req2.closeRequest())

